extends Node3D

@export var angle_of_attack:float = 0
@export var camber:float = 0.1
@export var span:float = 1

@onready var wind:RayCast3D = $wind
@onready var lift:RayCast3D = $lift
@onready var drag:RayCast3D = $drag
@onready var attk:RayCast3D = $attack


# Called when the node enters the scene tree for the first time.
func _ready():
    pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
    
    angle_of_attack = Input.get_action_strength("pitch_up")  - Input.get_action_strength("pitch_down")
    
    attk.target_position = Vector3.FORWARD.rotated(Vector3.LEFT, angle_of_attack)
    
    var drag_strength = span * - wind.target_position.cross(attk.target_position.rotated(Vector3.LEFT, - camber))
    
    drag.target_position = - drag_strength.length() * wind.target_position
    
    lift.target_position = - drag_strength.cross(attk.target_position)
