extends RigidBody3D

@export var default_trim: Vector2 = Vector2.ZERO
@export var trim_speed: Vector2 = Vector2(1, 1)
@export var trim_limits: Vector2 = Vector2(1, 1)
@onready var current_trim: Vector2 = default_trim

# Called when the node enters the scene tree for the first time.
func _ready():
    pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
    # process trim input
    current_trim += Vector2(Input.get_action_strength("trim_pitch_up") - Input.get_action_strength("trim_pitch_down"),
                            Input.get_action_strength("trim_left") - Input.get_action_strength("trim_right")) * delta * trim_speed
    
    current_trim = current_trim.clamp(default_trim - trim_limits, default_trim + trim_limits)
    
    # set ui stuff
    $trim/vertical.value = (current_trim.x - default_trim.x) * 1/trim_limits.x
    $trim/horizontal.value = - (current_trim.y - default_trim.y) * 1/trim_limits.y
